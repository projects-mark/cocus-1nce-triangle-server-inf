#!/usr/bin/env node
import 'source-map-support/register';
import * as cdk from 'aws-cdk-lib';
import { Cocus1NceTriangleServerInfStack } from '../lib/cocus-1nce-triangle-server-inf-stack';
import { env } from 'process';

const app = new cdk.App();
new Cocus1NceTriangleServerInfStack(app, 'Cocus1NceTriangleServerInfStack', {

  /* Uncomment the next line to specialize this stack for the AWS Account
   * and Region that are implied by the current CLI configuration. */
  env: { account: process.env.AWS_ACCOUNT, region: 'eu-west-1' }

  /* For more information, see https://docs.aws.amazon.com/cdk/latest/guide/environments.html */
});