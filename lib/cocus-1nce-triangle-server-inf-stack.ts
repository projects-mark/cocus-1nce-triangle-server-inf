import { aws_ec2, aws_ecs, aws_ecs_patterns, aws_rds, aws_secretsmanager, Duration, Stack, StackProps } from 'aws-cdk-lib';
import { SecurityGroup } from 'aws-cdk-lib/aws-ec2';
import { Construct } from 'constructs';

export class Cocus1NceTriangleServerInfStack extends Stack {
  constructor(scope: Construct, id: string, props?: StackProps) {
    super(scope, id, props);

    // Our Virtual Private Cloud
    const vpc = new aws_ec2.Vpc(this, 'cocus_1nce_vpc', {
      maxAzs: 2,
      natGateways: 1
    })

    // Config for ECS Cluster
    const restWSCluster = new aws_ecs.Cluster(this, 'cocus-1nce-triangle-ecs',  {
      vpc,
      clusterName: "cocus_1nce_triangle_cluster"
    }) 

    // Letting secret manager assist us in generating database security
    const databasePassword = new aws_secretsmanager.Secret(this, 'db_secret',  {
      secretName: "cocus_1nce_db1_db_secret",
      generateSecretString: {
        excludePunctuation: true
      }
    });

    // Creating a security group
    const databaseSecurityGroup = new SecurityGroup(this, 'cocus_1nce_SG', {
      vpc, 
      description: "MySQL Database Security Group"
    })

    databaseSecurityGroup.addIngressRule(aws_ec2.Peer.anyIpv4(), aws_ec2.Port.tcp(3306));

    // We are setting our Aurora database config, provisioning our servless resource, completely managed database by AWS
    const auroraServerlessRds = new aws_rds.CfnDBCluster(this, "cocus_1nce_serverless", {
      engine: aws_rds.DatabaseClusterEngine.AURORA_MYSQL.engineType,
      engineVersion: aws_rds.AuroraMysqlEngineVersion.VER_2_10_1.auroraMysqlMajorVersion,
      engineMode: 'serverless',
      databaseName: 'cocus_1nce',
      dbClusterIdentifier: "cocus-1nce-cluster-id",
      masterUsername: 'dataMngr',
      masterUserPassword: databasePassword.secretValue.toString(),

      dbSubnetGroupName: new aws_rds.CfnDBSubnetGroup(this, "db-subnet-group", {
        dbSubnetGroupDescription: `cocus_1nce_cluster subnet group`,
        subnetIds: vpc.selectSubnets({ subnetType: aws_ec2.SubnetType.PRIVATE }).subnetIds
      }).ref,
      vpcSecurityGroupIds: [databaseSecurityGroup.securityGroupId],

      storageEncrypted: true,
      deletionProtection: false,
      backupRetentionPeriod: 14,
      port: 3306,

      scalingConfiguration: {
        autoPause: true,
        secondsUntilAutoPause: 900,
        minCapacity: 1,
        maxCapacity: 4
      }
    })

    //Our application in AWS Fargate + ALB
    const triangleRestWS = new aws_ecs_patterns.ApplicationLoadBalancedFargateService(this, 'triangleClassificationRestSpringBoot app svc', {
      cluster: restWSCluster,
      desiredCount: 1,
      cpu: 1024,
      memoryLimitMiB: 2048,
      taskImageOptions: {
        // Referring to our Springboot artifact
        image: aws_ecs.ContainerImage.fromAsset('../cocus-1nce-triangle-rest-ws'),
        containerPort: 19041,
        environment: {
          'jdbc_url': `jdbc:mysql://` + auroraServerlessRds.attrEndpointAddress + `:3306/cocus_1nce?autoReconnect=true&useUnicode=true&characterEncoding=UTF-8&useSSL=false&allowPublicKeyRetrieval=true`,
          'jdbc_username': 'dataMngr',
         },
        secrets: {
          'jdbc_password': aws_ecs.Secret.fromSecretsManager(databasePassword)
        }
      }
    })
    
    // Customize healthcheck on ALB, assiting us in managing our load 
    triangleRestWS.targetGroup.configureHealthCheck({
      "port": 'traffic-port',
      "path": '/',
      "interval": Duration.seconds(5),
      "timeout": Duration.seconds(4),
      "healthyThresholdCount": 2,
      "unhealthyThresholdCount": 2,
      "healthyHttpCodes": "200,301,302"
    })
    
    // Autoscaling - cpu
    const triangleRestWSAutoScaling = triangleRestWS.service.autoScaleTaskCount({
      maxCapacity: 6,
      minCapacity: 1
    })
    
    triangleRestWSAutoScaling.scaleOnCpuUtilization('CpuScaling', {
      targetUtilizationPercent: 45,
      policyName: "cpu autoscaling",
      scaleInCooldown: Duration.seconds(30),
      scaleOutCooldown: Duration.seconds(30)
    })    
  }
}
