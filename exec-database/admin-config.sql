create database if not exists cocus_1nce_test;
create user 'cocus_test_dba'@'%' identified by 'testingSecretRecipe';
grant all privileges on cocus_1nce_test.* to 'cocus_test_dba'@'%';
flush privileges;

create database if not exists cocus_1nce;
create user 'cocus_schema_manager'@'%' identified by 's3cretP@$$w0rd';
grant all privileges on cocus_1nce.* to 'cocus_schema_manager'@'%';
flush privileges;
